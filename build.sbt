val Http4sVersion = "0.21.31"
val http4sVersion = "0.23.0-RC1"

import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

val tylipPublic =
  "tylip-public" at "https://tylip.jfrog.io/artifactory/tylip-public/"

val hsostarter =
  crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Full).in(file("."))
    .settings(
      organization := "ru.psttf",
      scalaVersion := "2.13.5",
      version := "0.1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        "org.typelevel" %%% "cats-effect" % "2.1.4",
        "org.typelevel" %%% "mouse" % "1.0.4",
        "io.circe" %%% "circe-generic" % "0.14.1",
        "io.circe" %%% "circe-literal" % "0.14.1",
        "io.circe" %%% "circe-generic-extras" % "0.14.1",
        "io.circe" %%% "circe-parser" % "0.14.1",
        "com.outr" %%% "scribe" % "3.6.4",
      ),
      scalacOptions ++= Seq(
        "-Ymacro-annotations",
        "-Xlint:-byname-implicit,_",
        "-unchecked",
        "-deprecation",
        "-feature",
        "-language:higherKinds",
      ),
      scalafmtOnCompile := !insideCI.value,
    )
    .jvmSettings(Seq(
    ))

val hsostarterJS = hsostarter.js
  .enablePlugins(ScalaJSBundlerPlugin)
  .disablePlugins(RevolverPlugin)
  .settings(
    resolvers ++= Seq(
      //Resolver.sonatypeRepo("releases"),
      "jitpack" at "https://jitpack.io",
      //Resolver.sonatypeRepo("snapshots"),
    ),
    libraryDependencies ++= Seq(
      "io.github.outwatch" %%% "outwatch" % "1.0.0-RC3",
      "org.typelevel" %%% "simulacrum" % "1.0.0",
      //"org.scala-js" %%% "scalajs-dom" % "1.2.0",
      // "org.typelevel" %% "cats-effect" % "3.3.1",
      //"com.github.OutWatch.outwatch" %%% "outwatch" % "584f3f2c32",
      "com.lihaoyi" %%% "fastparse" % "2.3.2",
      "org.scalatest" %%% "scalatest" % "3.2.2" % "test",
      "com.github.scopt" %%% "scopt" % "4.0.1",
      "com.beachape" %%% "enumeratum" % "1.7.0",
      "io.monix" %%% "monix" % "3.4.0",
      //"org.scala-js" %%% "scalajs-dom" % "2.0.0",
      //"fr.hmil" %%% "roshttp" % "3.0.0",
      //"org.scala-js" %%% "scalajs-dom" % "0.9.8",
      //"org.endpoints4s" %%% "xhr-client" % "4.0.0",
      //"org.http4s" %%% "http4s-client" % "0.23.7"
    ),

    npmDependencies in Compile ++= Seq(
      "jquery" -> "3.3",
      "bootstrap" -> "4.3",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(), // LibraryOnly() for faster dev builds
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("hsostarter.js.UifierIOApp"),
    useYarn := true, // makes scalajs-bundler use yarn instead of npm

  )

val hsostarterJVM = hsostarter.jvm
  .enablePlugins(JavaAppPackaging)
  .settings(
    resolvers += tylipPublic,
    (unmanagedResourceDirectories in Compile) += (resourceDirectory in(hsostarterJS, Compile)).value,
    mappings.in(Universal) ++= webpack.in(Compile, fullOptJS).in(hsostarterJS, Compile).value.map { f =>
      f.data -> s"assets/${f.data.getName}"
    },
    mappings.in(Universal) ++= Seq(
      (target in(hsostarterJS, Compile)).value / ("scala-2.13") / "scalajs-bundler" / "main" / "hsostarter-fastopt-library.js" ->
        "assets/hsostarter-fastopt-library.js",
      (target in(hsostarterJS, Compile)).value / ("scala-2.13") / "scalajs-bundler" / "main" / "hsostarter-fastopt-loader.js" ->
        "assets/hsostarter-fastopt-loader.js",
      (target in(hsostarterJS, Compile)).value / ("scala-2.13") / "scalajs-bundler" / "main" / "hsostarted-fastopt.js" ->
        "assets/hsostarted-fastopt.js",
    ),
    bashScriptExtraDefines += """addJava "-Dassets=${app_home}/../assets"""",
    mainClass in reStart := Some("hsostarter.jvm.HSOStarterJVM"),
    libraryDependencies ++= Seq(
      "com.lihaoyi" %% "fastparse" % "2.3.2",
      "org.scalatest" %% "scalatest" % "3.2.2" % "test",
      "com.github.scopt" %% "scopt" % "4.0.0-RC2",
      "org.postgresql" % "postgresql" % "42.3.1",
      "com.typesafe.slick" %% "slick-hikaricp" % "3.3.3",
      "com.github.tminglei" %% "slick-pg" % "0.20.2",
      "com.github.tminglei" %% "slick-pg_circe-json" % "0.20.2",
      "org.flywaydb" % "flyway-core" % "8.2.3",
      "org.http4s" %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s" %% "http4s-circe" % Http4sVersion,
      "org.http4s" %% "http4s-dsl" % Http4sVersion,
      "org.http4s" %% "http4s-blaze-client" % Http4sVersion,
      "ch.qos.logback" % "logback-classic" % "1.2.9",
      "com.github.pureconfig" %% "pureconfig" % "0.14.1",
      "com.github.pureconfig" %% "pureconfig-cats-effect" % "0.14.1",
      "com.tylip" %% "cats-effect-syntax" % "0.1.3",
      "org.slf4j" % "slf4j-nop" % "1.7.32",
      "io.circe" %% "circe-parser" % "0.14.1",
      "com.github.jwt-scala" %% "jwt-circe" % "9.0.3"




    ),
    scalacOptions += "-Wconf:cat=lint-multiarg-infix:s",
  )

disablePlugins(RevolverPlugin)

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}

herokuAppName in Compile := "http4s-slick-outwatch-starter"

target in Compile := (target in(hsostarterJVM, Compile)).value
