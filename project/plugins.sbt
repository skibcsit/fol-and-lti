addSbtPlugin("org.portable-scala" % "sbt-scalajs-crossproject" % "1.1.0")


addSbtPlugin("org.scala-js" % "sbt-scalajs" % "1.8.0")


//addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.33")


//addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.0.0+1-d083e6df-SNAPSHOT")
addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.20.0")
// we target Scala.js 0.6 because of cats: https://github.com/typelevel/cats/issues/2195
//addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.15.0-0.6") // scala-steward:off
//addSbtPlugin("ch.epfl.scala" % "sbt-scalajs-bundler" % "0.0.0+1-d083e6df-SNAPSHOT")
addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.9.7")
addSbtPlugin("io.spray" % "sbt-revolver" % "0.9.1")
addSbtPlugin("com.heroku" % "sbt-heroku" % "2.1.4")
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.4.5")
