package hsostarter.models

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

final case class lti1p3(
  score: Int,
)

object lti1p3 {
  implicit val helloEncoder: Encoder[lti1p3] = deriveEncoder
  implicit val helloDecoder: Decoder[lti1p3] = deriveDecoder
}
