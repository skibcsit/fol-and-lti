module.exports = {
  "require": (function(x0) {
    return {
      "snabbdom/modules/class": require("snabbdom/modules/class"),
      "snabbdom/modules/style": require("snabbdom/modules/style"),
      "snabbdom/modules/eventlisteners": require("snabbdom/modules/eventlisteners"),
      "snabbdom/modules/props": require("snabbdom/modules/props"),
      "snabbdom": require("snabbdom"),
      "snabbdom/tovnode": require("snabbdom/tovnode"),
      "snabbdom/modules/attributes": require("snabbdom/modules/attributes")
    }[x0]
  })
}