package hsostarter.jvm.http

import java.time.Instant
import org.http4s.client.dsl.io._
import org.http4s.Method._
import cats.effect.{Blocker, ContextShift, IO}
import hsostarter.models.{isnorm, lti1p3, Hello}
import io.circe.syntax._
import org.http4s.{EntityDecoder, HttpRoutes, UrlForm}
import org.http4s.circe._
import org.http4s.dsl.{request, Http4sDsl}
import io.circe._
import io.circe.parser._
import org.http4s.implicits._
import io.circe._
import io.circe.parser._
import org.http4s.headers._
import org.http4s.headers.`Content-Type`
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, ExecutionContext}
import io.circe.generic.auto._
import io.circe.literal.JsonStringContext
import io.circe.syntax._
import org.http4s._
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe._
import org.http4s.client.JavaNetClientBuilder
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.io._
import org.http4s.headers._
import org.http4s.MediaType
import org.http4s.Method._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.ExecutionContext
import java.util.concurrent._
import cats.effect.Blocker
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64
import hsostarter.jvm.http.HelloEndpoints.QueryParamDecoderMatcher
import org.checkerframework.common.reflection.qual.GetClass
import org.http4s.headers._

import java.util.concurrent._
import javax.swing.text.html.HTML
import scala.util.Random
import org.http4s.Uri
import org.http4s.client.middleware.FollowRedirect
import org.http4s.headers._

import java.time.Instant
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

object lti extends Http4sDsl[IO] {

  case class Lti13Login(
    iss: String,
    target_link_uri: Option[String],
    login_hint: Option[String],
    lti_message_hint: Option[String],
    client_id: Option[String],
    lti_deployment_id: Option[String],
  )
  case class login13(
    iss: String,
    targetLinkUri: Option[String],
    loginHint: Option[String],
    ltiMessageHint: Option[String],
    clientId: Option[String],
    ltiDeploymentId: Option[String],
  )
  implicit val ioContextShift: ContextShift[IO] =
    IO.contextShift(ExecutionContext.global)

  // implicit val decoder: EntityDecoder[IO, Json] = jsonOf[IO, Json]
  final protected val nonceBuffer: Array[Byte] = new Array[Byte](16)

  def generateNonce = synchronized {
    scala.util.Random.nextBytes(nonceBuffer)
    // let's use base64 encoding over hex, slightly more compact than hex or decimals
    Base64.encode(nonceBuffer)
  }
  val safeChars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwqyx0123456789"
  def safeString(n: Int): String =
    (0 until n).map { _ =>
      safeChars(Random.nextInt(safeChars.length))
    }.mkString

  def jwtmake = {
    val calim = JwtClaim(
      "",
      Some("http://localhost:8080"),
      Some("LV1DIR7RZVERBoY"),
      Some(Set("http://localhost:8080/mod/lti/token.php")),
      Some(Instant.now.getEpochSecond),
      None,
      Some(Instant.now.getEpochSecond + 3600),
      None,
    )

    val key =
      "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAu1SU1LfVLPHCozMxH2Mo\n4lgOEePzNm0tRgeLezV6ffAt0gunVTLw7onLRnrq0/IzW7yWR7QkrmBL7jTKEn5u\n+qKhbwKfBstIs+bMY2Zkp18gnTxKLxoS2tFczGkPLPgizskuemMghRniWaoLcyeh\nkd3qqGElvW/VDL5AaWTg0nLVkjRo9z+40RQzuVaE8AkAFmxZzow3x+VJYKdjykkJ\n0iT9wCS0DRTXu269V264Vf/3jvredZiKRkgwlL9xNAwxXFg0x/XFw005UWVRIkdg\ncKWTjpBP2dPwVZ4WWC+9aGVd+Gyn1o0CLelf4rEjGoXbAAEgAqeGUxrcIlbjXfbc\nmwIDAQAB\n-----END PUBLIC KEY-----"

    val algo = JwtAlgorithm.RS256
    val token = JwtCirce.encode(calim, key, algo)
    token
  }

  def accessreq = {
    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create
    val req =
      POST(
        UrlForm(
          "grant_type" -> "client_credentials",
          "client_assertion_type" -> "urn:ietf:params:oauth:client-assertion-type:jwt-bearer",
          "client_assertion" -> jwtmake,
          "scope" -> "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem https://purl.imsglobal.org/spec/lti-ags/scope/lineitem.readonly https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly https://purl.imsglobal.org/spec/lti-ags/scope/score",
        ),
        uri"http://localhost:8080/mod/lti/token.php",
      )
    val accessreq = httpClient.expect[String](req)
    accessreq.unsafeRunSync()
  }

  def sendscore(score: Int) = {
    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create

    val data =
      s"""
         |{
         |  "activityProgress": "Completed",
         |  "comment": "string",
         |  "gradingProgress": "FullyGraded",
         |  "scoreGiven": $score,
         |  "scoreMaximum": 100,
         |  "timestamp": "2022-01-31T23:24:33.336Z",
         |  "userId": "3"
         |}
         |
         |""".stripMargin

    val dataparsed = parse(data)

    val authorizationHeader = Headers.of(
      Authorization(
        Credentials.Token(AuthScheme.Bearer, "cb993e106ea7396c7dcae96c5a81f316"),
      ),
    )

    val body =
      json"""{"activityProgress": "Completed","comment": "Nice!","gradingProgress": "FullyGraded","scoreGiven": $score,"scoreMaximum": 100,"timestamp": "2022-02-02T19:32:33.336Z","userId": "3"}"""
    val mymedia =
      new MediaType("application", "vnd.ims.lis.v1.score+json")
    val myrequest = Request[IO](
      Method.POST,
      uri"http://localhost:8080/mod/lti/services.php/2/lineitems/2/lineitem/scores?type_id=1",
      HttpVersion.`HTTP/1.1`,
      headers = authorizationHeader,
    ).withEntity(body).withContentType(
        `Content-Type`(mymedia),
      )

    val postreq = httpClient.expect[String](
      myrequest,
    )
    postreq.unsafeRunSync()

  }

  def sendgettolms(
    clientid: String,
    targetlink: String,
    randstate: String,
    randnonce: String,
    loginhint: String,
    lmh: String,
  ) = {

    val blockingPool = Executors.newFixedThreadPool(5)
    val blocker = Blocker.liftExecutorService(blockingPool)
    val httpClient = JavaNetClientBuilder[IO](blocker).create
    // val link =
    //  s"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh"

    val request = POST(
      UrlForm(
        ("scope", "openid"),
        ("response_type", "id_token"),
        ("response_mode", "form_post"),
        ("prompt", "none"),
        ("client_id", "7bw779F24uuwy0X"),
        ("redirect_uri", "http://127.0.0.1:8090/login"),
        ("state", "NoCqoVOk7RR1HLQEWo7vRlpq"),
        ("login_hint", "2"),
        ("lti_message_hint", "15"),
      ),
      uri"http://localhost:8080/mod/lti/auth.php?",
      Accept(MediaType.application.json),
    )

    val req = GET(
      // uri"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh",
      // uri"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http%3A%2F%2F127.0.0.1%3A8090%2F&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      uri"http://localhost:8080/mod/lti/auth.php?"
        .withQueryParam("scope", "openid").withQueryParam(
          "response_type",
          "id_token",
        ).withQueryParam("response_mode", "form_post").withQueryParam(
          "prompt",
          "none",
        ).withQueryParam("client_id", clientid).withQueryParam(
          "redirect_uri",
          "http://127.0.0.1:8090/",
        ).withQueryParam("state", randstate).withQueryParam(
          "nonce",
          randnonce,
        ).withQueryParam("login_hint", loginhint).withQueryParam(
          "lti_message_hint",
          lmh,
        ),
    )
    val postval =
      """
        |{
        |"scope":"openid",
        |"response_type":"id_token",
        |"response_mode":"form_post",
        |"prompt":"none",
        |"client_id":"7bw779F24uuwy0X",
        |"redirect_uri":"http%3A%2F%2F127.0.0.1%3A8090%2F",
        |"state":"NoCqoVOk7RR1HLQEWo7vRlpq",
        |"nonce":"fvwapGVEa4gBNpEU6mep4GaY",
        |"login_hint": 2,
        |"lti_message_hint": 15
        |
        |}
        |"""

    val jsonpostval = parse(postval)

    val getreq = httpClient.expect[String](
      req,
      // request,
      // s"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh",
      // uri"localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http://127.0.0.1:8090/&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      // uri"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=7bw779F24uuwy0X&redirect_uri=http://127.0.0.1:8090/login&state=NoCqoVOk7RR1HLQEWo7vRlpq&nonce=fvwapGVEa4gBNpEU6mep4GaY&login_hint=2&lti_message_hint=15",
      // "http://127.0.0.1:8090/ispnf?answer=asda&randfor=asda",
    )
    getreq.unsafeRunSync()

    // req

  }

  def endpoints(): HttpRoutes[IO] =
    HttpRoutes.of[IO] {
      case request @ POST -> Root / "login" =>
        // for {
        // lti13 <- request.as[String]

        // resp <- Ok(
        // login13(
        // parse(lti13).,
        // lti13.target_link_uri,
        // lti13.login_hint,
        // lti13.lti_message_hint,
        // lti13.client_id,
        // lti13.lti_deployment_id,
        // ).asJson,
        // )
        // for {
        // iss <- request.params.get("iss")
        // targeturl <- request.params.get("target_link_uri")
        // loginhint <- request.params.get("login_hint")
        // ltimeshint <- request.params.get("lti_message_hint")
        // clientid <- request.params.get("client_id")
        // ltiDeploymentId <- request.params.get("lti_deployment_id")

        // } yield {

        //  login13(iss,Some(targeturl),Some(loginhint),Some(ltimeshint),Some(clientid),Some(ltiDeploymentId))
        // }
        // val resource = for {
        // client <- BlazeClientBuilder[IO](scala.concurrent.ExecutionContext.Implicits.global).resource
        // } yield client
        // resource.use(client => client.expect[String](Uri.unsafefromString("http://localhost:8080"))).unsafeRunSync()
        // val response = client.expect[String](Uri.unsafefromString("http://localhost:8888/hello"))

        val str = request.as[String].unsafeRunSync()
        val clientid = str.substring(126, 141)
        val loginhint = str.substring(94, 95)
        val lmh = str.substring(113, 115)
        val targetlink = str.substring(50, 82)
        // val targetlink = "http://127.0.0.1:8090/login"
        val randstate = safeString(28)
        val randnonce = safeString(30)
        val link =
          s"http://localhost:8080/mod/lti/auth.php?scope=openid&response_type=id_token&response_mode=form_post&prompt=none&client_id=$clientid&redirect_uri=$targetlink&state=$randstate&nonce=$randnonce&login_hint=$loginhint&lti_message_hint=$lmh"
        // val validUri = Uri.fromString(link).toOption.get

        // val requestget: Request[IO] = Request[IO](Method.GET, validUri)
        // httpClient.run(requestget)
        // Ok(accessreq)

        Ok(
          sendgettolms(
            clientid,
            targetlink,
            randstate,
            randnonce,
            loginhint,
            lmh,
          ),
          `Content-Type`(MediaType.text.html),
          // sendgettolms(),
        )
        PermanentRedirect(
          Location(
            uri"http://localhost:8080/mod/lti/auth.php?"
              .withQueryParam("scope", "openid").withQueryParam(
                "response_type",
                "id_token",
              ).withQueryParam("response_mode", "form_post").withQueryParam(
                "prompt",
                "none",
              ).withQueryParam("client_id", clientid).withQueryParam(
                "redirect_uri",
                "http://127.0.0.1:8090/",
              ).withQueryParam("state", randstate).withQueryParam(
                "nonce",
                randnonce,
              ).withQueryParam("login_hint", loginhint).withQueryParam(
                "lti_message_hint",
                lmh,
              ),
          ),
        )
      // TemporaryRedirect(Uri(path = "/"))
      // } yield Ok(lti13)

      case request @ POST -> Root / "scores" =>
        Ok(sendscore(80))

    }

}
