package hsostarter.jvm

import cats.effect._
import cats.syntax.semigroupk._
import com.tylip.catseffect.syntax._
import hsostarter.jvm.config.AppConfig
import hsostarter.jvm.http._
import org.http4s.implicits._
import org.http4s.server.Server
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import pureconfig.ConfigSource
import pureconfig.module.catseffect.syntax._
import scribe.{Level, Logger}

import scala.concurrent.ExecutionContext

object HSOStarterJVM extends IOApp {

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- IO(
        Logger.root
          .clearHandlers().clearModifiers()
          .withHandler(minimumLevel = Some(Level.Debug))
          .replace(),
      )
      exitCode <- server
        .use(_ => IO.never)
        .as(ExitCode.Success)
    } yield exitCode

  private def server: Resource[IO, Server[IO]] =
    for {
      blocker   <- Blocker[IO]
      appConfig <- ConfigSource.default.loadF[IO, AppConfig](blocker).resource
      staticEndpoints = new StaticEndpoints[IO](appConfig.assets, blocker)
      httpApp = (
        staticEndpoints.endpoints() <+> HelloEndpoints.endpoints() <+> isNormal
          .endpoints() <+> compare.endpoints() <+> lti.endpoints()
      ).orNotFound
      server <- BlazeServerBuilder[IO](ExecutionContext.global)
        .bindHttp(8090, "localhost")
        .withHttpApp(CORS.policy(httpApp))
        .resource
    } yield server

}
